﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TurtlePop
{
    public partial class TurtleBoard : UserControl
    {
        private List<List<TurtleCell>> renderTarget = new List<List<TurtleCell>>();
        private Dictionary<BoardCellState, string> renderTable = new Dictionary<BoardCellState, string>();
        private int highlightedRegion = -1;
        public int HighlightedRegion
        {
            get
            {
                return highlightedRegion;
            }
            set
            {
                highlightedRegion = value;
                RenderBoard();
            }
        }
        private List<TurtleCell> CreateRenderTargetColumn(TurtleColumn turtleColumn)
        {
            List<TurtleCell> result = new List<TurtleCell>();
            result.Add(turtleColumn.cell0);
            result.Add(turtleColumn.cell1);
            result.Add(turtleColumn.cell2);
            result.Add(turtleColumn.cell3);
            result.Add(turtleColumn.cell4);
            result.Add(turtleColumn.cell5);
            result.Add(turtleColumn.cell6);
            result.Add(turtleColumn.cell7);
            result.Add(turtleColumn.cell8);
            result.Add(turtleColumn.cell9);
            result.Add(turtleColumn.cell10);
            result.Add(turtleColumn.cell11);
            result.Add(turtleColumn.cell12);
            result.Add(turtleColumn.cell13);
            result.Add(turtleColumn.cell14);
            return result;
        }
        private void InitializeRenderTarget()
        {
            renderTarget.Add(CreateRenderTargetColumn(column0));
            renderTarget.Add(CreateRenderTargetColumn(column1));
            renderTarget.Add(CreateRenderTargetColumn(column2));
            renderTarget.Add(CreateRenderTargetColumn(column3));
            renderTarget.Add(CreateRenderTargetColumn(column4));
            renderTarget.Add(CreateRenderTargetColumn(column5));
            renderTarget.Add(CreateRenderTargetColumn(column6));
            renderTarget.Add(CreateRenderTargetColumn(column7));
            renderTarget.Add(CreateRenderTargetColumn(column8));
            renderTarget.Add(CreateRenderTargetColumn(column9));
            renderTarget.Add(CreateRenderTargetColumn(column10));
            renderTarget.Add(CreateRenderTargetColumn(column11));
            renderTarget.Add(CreateRenderTargetColumn(column12));
            renderTarget.Add(CreateRenderTargetColumn(column13));
            renderTarget.Add(CreateRenderTargetColumn(column14));
            renderTarget.Add(CreateRenderTargetColumn(column15));
            renderTarget.Add(CreateRenderTargetColumn(column16));
            renderTarget.Add(CreateRenderTargetColumn(column17));
            renderTarget.Add(CreateRenderTargetColumn(column18));
            renderTarget.Add(CreateRenderTargetColumn(column19));
        }
        public TurtleBoard()
        {
            InitializeComponent();
            column0.Column = 0;
            column1.Column = 1;
            column2.Column = 2;
            column3.Column = 3;
            column4.Column = 4;
            column5.Column = 5;
            column6.Column = 6;
            column7.Column = 7;
            column8.Column = 8;
            column9.Column = 9;
            column10.Column = 10;
            column11.Column = 11;
            column12.Column = 12;
            column13.Column = 13;
            column14.Column = 14;
            column15.Column = 15;
            column16.Column = 16;
            column17.Column = 17;
            column18.Column = 18;
            column19.Column = 19;
            InitializeRenderTarget();
            ReloadTheme();
        }
        public void ReloadTheme()
        {
            Theme theme = GameConfig.Themes[GameConfig.CurrentTheme];
            renderTable.Clear();
            renderTable.Add(BoardCellState.Empty, theme.GetImageUri(BoardCellState.Empty));
            renderTable.Add(BoardCellState.Remove, theme.GetImageUri(BoardCellState.Remove));
            renderTable.Add(BoardCellState.First, theme.GetImageUri(BoardCellState.First));
            renderTable.Add(BoardCellState.Second, theme.GetImageUri(BoardCellState.Second));
            renderTable.Add(BoardCellState.Third, theme.GetImageUri(BoardCellState.Third));
            renderTable.Add(BoardCellState.Fourth, theme.GetImageUri(BoardCellState.Fourth));
            renderTable.Add(BoardCellState.Fifth, theme.GetImageUri(BoardCellState.Fifth));
            renderTable.Add(BoardCellState.Sixth, theme.GetImageUri(BoardCellState.Sixth));
        }
        public void RenderBoard()
        {
            for (int column = 0; column < GameData.Board.Columns; ++column)
            {
                for (int row = 0; row < GameData.Board.Rows; ++row)
                {
                    renderTarget[column][row].layer0.Source = BitmapImageManager.GetImage(renderTable[GameData.Board[column][row].State]);
                    if (GameData.Board[column][row].Region == HighlightedRegion && GameData.Board[column][row].State != BoardCellState.Empty)
                    {
                        renderTarget[column][row].layer1.Visibility = Visibility.Visible;
                        if (GameData.Board[column][row].Isolated)
                        {
                            renderTarget[column][row].layer1.Fill = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));
                        }
                        else
                        {
                            renderTarget[column][row].layer1.Fill = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));
                        }
                    }
                    else
                    {
                        renderTarget[column][row].layer1.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }
    }
}
