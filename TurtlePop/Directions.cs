﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public static class Directions
    {
        public const int North = 0;
        public const int East = 1;
        public const int South = 2;
        public const int West = 3;
        public const int First = North;
        public const int Last = West;
        public const int Count = 4;
        private static int[] deltaX = new int[] { 0, 1, 0, -1 };
        private static int[] deltaY = new int[] { -1, 0, 1, 0 };
        private static int[] opposite = new int[] { South, West, North, East };
        public static int[] DeltaX
        {
            get
            {
                return deltaX;
            }
        }
        public static int[] DeltaY
        {
            get
            {
                return deltaY;
            }
        }
        public static int[] Opposite
        {
            get
            {
                return opposite;
            }
        }
    }
}
