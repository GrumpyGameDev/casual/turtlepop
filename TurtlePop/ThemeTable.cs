﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Collections.Generic;

namespace TurtlePop
{
    public class ThemeTable
    {
        private List<Theme> themes = new List<Theme>();
        public Theme this[int index]
        {
            get
            {
                return themes[index];
            }
        }
        public int Count
        {
            get
            {
                return themes.Count;
            }
        }
        private ThemeTable() { }
        public ThemeTable(XElement theThemesNode)
        {
            foreach (XElement themeNode in theThemesNode.Elements("theme"))
            {
                themes.Add(new Theme(themeNode));
            }
        }
    }
}
