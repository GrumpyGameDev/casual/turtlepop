﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Collections.Generic;

namespace TurtlePop
{
    public class Theme
    {
        private string name = string.Empty;
        private string iconUri = string.Empty;
        private Dictionary<BoardCellState, string> imageUris = new Dictionary<BoardCellState, string>();
        public string Name
        {
            get
            {
                return name;
            }
        }
        public string IconUri
        {
            get
            {
                return iconUri;
            }
        }
        public string GetImageUri(BoardCellState theState)
        {
            return imageUris[theState];
        }
        public Theme(XElement theThemeNode)
        {
            name = theThemeNode.Element("name").Value;
            iconUri = theThemeNode.Element("iconUri").Value;
            foreach(XElement imageElement in theThemeNode.Element("images").Elements())
            {
                BoardCellState state = (BoardCellState)Enum.Parse(typeof(BoardCellState), imageElement.Name.LocalName, false);
                imageUris[state] = imageElement.Value;
            }
        }
    }
}
