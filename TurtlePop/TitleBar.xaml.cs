﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public partial class TitleBar : UserControl
    {
        public void UpdateScores()
        {
            highRun.Text = GameData.HighScore.ToString();
            scoreRun.Text = GameData.Score.ToString();
        }
        public TitleBar()
        {
            InitializeComponent();
        }
    }
}
