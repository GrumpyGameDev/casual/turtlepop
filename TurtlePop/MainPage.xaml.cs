﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;

namespace TurtlePop
{
    public partial class MainPage : UserControl
    {
        private static MainPage singleton;
        public static MainPage Singleton
        {
            get
            {
                return singleton;
            }
        }
        private void MakeMoveCallback(int theColumn,int theRow)
        {
            titleBar.UpdateScores();
            rightBar.ShowMovesLeft();
            turtleBoard.HighlightedRegion = GameData.Board[theColumn][theRow].Region;
        }
        private void NewGameCallback()
        {
            turtleBoard.RenderBoard();
            rightBar.ShowMovesLeft();
            titleBar.UpdateScores();
        }
        public MainPage()
        {
            GameData.OnMakeMove += MakeMoveCallback;
            GameData.OnNewGame += NewGameCallback;
            singleton = this;
            InitializeComponent();
            GameData.NewGame();
            turtleBoard.RenderBoard();
            rightBar.ShowMovesLeft();
        }
    }
}
