﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public partial class TurtleColumn : UserControl
    {
        public int Column
        {
            get
            {
                return cell0.Column;
            }
            set
            {
                cell0.Column = value;
                cell1.Column = value;
                cell2.Column = value;
                cell3.Column = value;
                cell4.Column = value;
                cell5.Column = value;
                cell6.Column = value;
                cell7.Column = value;
                cell8.Column = value;
                cell9.Column = value;
                cell10.Column = value;
                cell11.Column = value;
                cell12.Column = value;
                cell13.Column = value;
                cell14.Column = value;
            }
        }
        public TurtleColumn()
        {
            InitializeComponent();
            cell0.Row = 0;
            cell1.Row = 1;
            cell2.Row = 2;
            cell3.Row = 3;
            cell4.Row = 4;
            cell5.Row = 5;
            cell6.Row = 6;
            cell7.Row = 7;
            cell8.Row = 8;
            cell9.Row = 9;
            cell10.Row = 10;
            cell11.Row = 11;
            cell12.Row = 12;
            cell13.Row = 13;
            cell14.Row = 14;
        }
    }
}
