﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public enum BoardCellState
    {
        Empty,
        Remove,
        First,
        Second,
        Third,
        Fourth,
        Fifth,
        Sixth
    }
    public class BoardCell
    {
        private BoardCellState state = BoardCellState.Empty;
        public BoardCellState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        private int region = -1;
        public int Region
        {
            get
            {
                return region;
            }
            set
            {
                region = value;
            }
        }
        private BoardCell[] neighbors = new BoardCell[Directions.Count];
        public BoardCell[] Neighbors
        {
            get
            {
                return neighbors;
            }
        }
        public void ClearState()
        {
            State = BoardCellState.Empty;
        }
        public void ClearRegion()
        {
            Region = -1;
        }
        public void Clear()
        {
            ClearState();
            ClearRegion();
        }
        public void PaintRegion(int theRegion)
        {
            if (theRegion != region)
            {
                region = theRegion;
                for (int direction = Directions.First; direction <= Directions.Last; ++direction)
                {
                    if (Neighbors[direction] != null)
                    {
                        if (Neighbors[direction].State == this.State)
                        {
                            Neighbors[direction].PaintRegion(theRegion);
                        }
                    }
                }
            }
        }
        public bool Isolated
        {
            get
            {
                for (int direction = Directions.First; direction <= Directions.Last; ++direction)
                {
                    if (Neighbors[direction] != null)
                    {
                        if (Neighbors[direction].State == this.State)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        public void CollapseDown()
        {
            if (Neighbors[Directions.North] == null)
            {
                State = BoardCellState.Empty;
            }
            else
            {
                State = Neighbors[Directions.North].State;
                Neighbors[Directions.North].CollapseDown();
            }
        }
        public void CollapseRight(bool bottomRow)
        {
            if (Neighbors[Directions.West] == null)
            {
                State = BoardCellState.Empty;
                if (Neighbors[Directions.North] != null)
                {
                    Neighbors[Directions.North].CollapseRight(false);
                }
            }
            else
            {
                State = Neighbors[Directions.West].State;
                if (Neighbors[Directions.North] != null)
                {
                    Neighbors[Directions.North].CollapseRight(false);
                }
                if (bottomRow)
                {
                    Neighbors[Directions.West].CollapseRight(true);
                }
            }
        }
    }
}
