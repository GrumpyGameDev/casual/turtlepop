﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace TurtlePop
{
    public static class GameConfig
    {
        private static ThemeTable themeTable = null;
        private static int currentTheme = 3;
        public static int CurrentTheme
        {
            get
            {
                return currentTheme;
            }
            set
            {
                if (value >= 0 && value < Themes.Count)
                {
                    currentTheme = value;
                }
            }
        }
        public static ThemeTable Themes
        {
            get
            {
                return themeTable;
            }
        }
        public static void Load()
        {
            XDocument document = XDocument.Load("config/themes.xml");
            themeTable = new ThemeTable(document.Element("themes"));
        }
        public static void Save()
        {
        }
        static GameConfig()
        {
            Load();
        }
    }
}
