﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public class BoardColumn
    {
        private BoardCell[] cells;
        public BoardCell this[int index]
        {
            get
            {
                if (index < 0) return null;
                if (index >= cells.Length) return null;
                return cells[index];
            }
        }
        public int Rows
        {
            get
            {
                return cells.Length;
            }
        }
        public BoardColumn(int theRows)
        {
            cells = new BoardCell[theRows];
            for (int row = 0; row < theRows; ++row)
            {
                cells[row] = new BoardCell();
            }
        }
        public void ClearStates()
        {
            foreach (BoardCell cell in cells)
            {
                cell.ClearState();
            }
        }
        public void ClearRegions()
        {
            foreach (BoardCell cell in cells)
            {
                cell.ClearRegion();
            }
        }
        public void Clear()
        {
            foreach (BoardCell cell in cells)
            {
                cell.Clear();
            }
        }
    }
}
