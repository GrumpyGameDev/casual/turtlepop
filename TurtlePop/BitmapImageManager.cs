﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Collections.Generic;

namespace TurtlePop
{
    public static class BitmapImageManager
    {
        private static Dictionary<string, BitmapImage> bitmapTable = new Dictionary<string, BitmapImage>();
        public static BitmapImage GetImage(string uri)
        {
            if (!bitmapTable.ContainsKey(uri))
            {
                bitmapTable.Add(uri, new BitmapImage(new Uri(uri, UriKind.Relative)));
            }
            return bitmapTable[uri];
        }
    }
}
