﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public partial class TurtleCell : UserControl
    {
        private int column;
        public int Column
        {
            get
            {
                return column;
            }
            set
            {
                column = value;
            }
        }
        private int row;
        public int Row
        {
            get
            {
                return row;
            }
            set
            {
                row = value;
            }
        }
        public TurtleCell()
        {
            InitializeComponent();
        }

        private void layer0_MouseEnter(object sender, MouseEventArgs e)
        {
            MainPage.Singleton.turtleBoard.HighlightedRegion = GameData.Board[Column][Row].Region;
        }

        private void layer1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            GameData.MakeMove(Column, Row);
        }
    }
}
