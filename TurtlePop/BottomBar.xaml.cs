﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TurtlePop
{
    public partial class BottomBar : UserControl
    {
        public BottomBar()
        {
            InitializeComponent();
        }

        private void newGameButton_Click(object sender, RoutedEventArgs e)
        {
            GameData.NewGame();
        }

        private void changeThemeButton_Click(object sender, RoutedEventArgs e)
        {
            GameConfig.CurrentTheme = (GameConfig.CurrentTheme + 1) % GameConfig.Themes.Count;
            MainPage.Singleton.turtleBoard.ReloadTheme();
            MainPage.Singleton.turtleBoard.RenderBoard();
        }
    }
}
